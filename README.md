# AmongUsPlus!

A plugin for Impostor v1.2.2.
This adds some commands and some Gamemodes (chart below)


# Commands

| Command        |Description               | Usage                     | Values                                                    |
|----------------|---------------------------|---------------------------|-----------------------------------------------------------|
|/color	(c)		 | Change your color         |/color "color"             | Colors: Red, Blue, Green, Pink, Orange, Yellow, Black, White, Purple, Brown, Cyan and Lime      |
|/name (n)		| Change your name          |/name "name"               | Name between 3 and 11 characters (exclusive range)        |
|/map (m)		| Change the map            |/map "map"                 | Maps: Skeld(1), Mira/MiraHQ(2), Polus(3)                  |
|/players (p)   | Change the max player quantity|/players "number"      | Number between 4 and 127 (inclusive range)                |
|/impostor (i)	| Change the max impostor quantity|/impostor "number"   | Number lesser than the half of the players                |
|/gamemodes (g) | Shows the information about the Gamemodes|/gamemodes [gamemode]               | [] Gamemodes: Black, Jester (based on Maartii's Jester mode)       |
|/help (h)		| Shows this chart ingame   |/help               |       |

