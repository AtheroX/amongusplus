﻿using Impostor.Api.Events;
using Impostor.Api.Plugins;
using my.AtheroX.AllOfUs.Handlers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace my.AtheroX.AOUStartup {
	public class ExamplePluginStartup : IPluginStartup {

		public void ConfigureHost(IHostBuilder host) {

		}

		public void ConfigureServices(IServiceCollection services) {
			services.AddSingleton<IEventListener, GameEventListener>();
		}
	}
}