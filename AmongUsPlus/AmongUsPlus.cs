﻿using System;
using System.Threading.Tasks;
using Impostor.Api.Events.Managers;
using Impostor.Api.Plugins;
using Microsoft.Extensions.Logging;

namespace my.AtheroX.AmongUsPlus
{
    [ImpostorPlugin(
        package: "my.AtheroX.AmongUsPlus",
        name: "AmongUsPlus",
        author: "AtheroX",
        version: "1.2.0")]
    public class AmongUsPlusPlugin : PluginBase
    {
        private readonly ILogger<AmongUsPlusPlugin> logger; 

        public AmongUsPlusPlugin(ILogger<AmongUsPlusPlugin> logger, IEventManager eventManager)
        {
            this.logger = logger;
        }

        public override ValueTask EnableAsync()
        {
            logger.LogInformation("LobbyCommands is starting.");
            return default;
        }

        public override ValueTask DisableAsync()
        {
            logger.LogInformation("LobbyCommands is being disabled.");
            return default;
        }
    }
}